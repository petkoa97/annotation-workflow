#!/usr/bin/bash
set -euo pipefail

# Log working directory for debugging
#echo "$PWD" > "./logs/post_deployment.log"


# Get TAMA package from github
if ! [[ -f ./tools/tama/tama_collapse.py ]]
then 
    rm -rf ./tools/tama && git clone https://github.com/GenomeRIK/tama.git ./tools/tama/
fi