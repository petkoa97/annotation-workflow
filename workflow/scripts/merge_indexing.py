# This script produces the index file necessary for TAMA merge
import sys
import pandas as pd
import re
import os

def num_sort(test_string):
    return list(map(int, re.findall(r'\d+', test_string)))[0]
    
bed_list= sys.argv[1].split() # the bed files to be merged
out_file= sys.argv[2]           # the name of the index file 



bed_list.sort(key=num_sort)  # seems expand() doesn't sort output



df = pd.DataFrame(columns=('file_name', 'cap_flag', 'merge_priority', 'source_name'))
for i in range(len(bed_list)):
    if not os.stat(bed_list[i]).st_size == 0 :
        df.loc[i] = [str(bed_list[i]),'no_cap','1,1,1','split_' +  str(i+1)]


df.to_csv(out_file, sep = '\t', index=False, header=False)