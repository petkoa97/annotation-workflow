import subprocess 
import os
import sys



def get_num_reads():   
  #  print(snakemake.input)
    result = subprocess.Popen(['samtools', 'view', '-c', inp ],stdout=subprocess.PIPE)
    output= result.communicate()[0]

    num_reads = int(output.split('\\')[0])

    return num_reads

def ceildiv(a, b):      # fancy math.ceil_div
    return -(a // -b)


def get_num_chunks():
    num_reads = get_num_reads()
    chunk_num = ceildiv(num_reads, int(chunk_size)*1000)
    return chunk_num



inp        = sys.argv[1]
out_dir    = sys.argv[2]
prefix     = sys.argv[3]
chunk_size = sys.argv[4]

input_path   = (os.getcwd()  + '/'+  inp)
out_dir_path = (os.getcwd()  + '/'+  out_dir)


isExist = os.path.isdir(out_dir_path)

chunk_num = get_num_chunks()

if not isExist:
    os.makedirs(out_dir_path)
    os.chdir(out_dir_path)  
    subprocess.call(['python', '../../../tools/tama/tama_go/split_files/tama_mapped_sam_splitter.py',input_path, str(chunk_num), prefix ] )
else:   
    raise FileNotFoundError("OUTPUT FOLDER ALREADY EXISTS")


