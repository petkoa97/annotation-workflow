import yaml 

def get_TAMA_Collapse_param_string():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            pstr = " "            

            # Check if custom string is supplied
            try: 
                pstr = data["TAMA_collapse_custom"]
                return pstr
            except:
                pass 

            # If not go through preset options
            
            if data["TAMA_collapse_presets"]["capped"] == True:
                pstr += " -x capped "   
            elif data["TAMA_collapse_presets"]["capped"] == False:
                pstr += " -x no_cap "
            
            if data["TAMA_collapse_presets"]["merge_dup"] == True:
                pstr += " -d merge_dup "  
            elif data["TAMA_collapse_presets"]["merge_dup"] == False:
                pstr += " -d no_merge "

            if data["TAMA_collapse_presets"]["sj"] == True:
                pstr += " -sj sj_priority"  
            elif data["TAMA_collapse_presets"]["sj"] == False:
                pstr += " -sj no_priority"
            
            if data["TAMA_collapse_presets"]["sjt"]:
                pstr += " -sjt " + str(data["TAMA_collapse_presets"]["sjt"])
            
            if data["TAMA_collapse_presets"]["lde"]:
                pstr += " -lde " + str(data["TAMA_collapse_presets"]["lde"])
            
            if data["TAMA_collapse_presets"]["a"]:
                pstr += " -a " + str(data["TAMA_collapse_presets"]["a"])

            if data["TAMA_collapse_presets"]["z"]:
                pstr += " -z " + str(data["TAMA_collapse_presets"]["z"])
            
            pstr += " -log log_off -b BAM -e longest_ends -icm ident_map  "
    
            return pstr


        except yaml.YAMLError as exc:
            print(exc + "\n SOMETHING IS WRONG WITH THE TAMA CONFIG")


def get_isoseq_collapse_param_string():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            pstr = " " 
            
            try: 
                pstr = data["ISOSEQ_COLLAPSE_CUSTOM"]
                return pstr
            except:
                pass 
            
            if data["ISOSEQ3_COLLAPSE_PARAMS"]["collapse_extra_5p_exons"] == False:
                pstr += " --do-not-collapse-extra-5exons "   
            if data["ISOSEQ3_COLLAPSE_PARAMS"]["fuzzy_junction"]:
                pstr += " --max-fuzzy-junction " + str(data["ISOSEQ3_COLLAPSE_PARAMS"]["fuzzy_junction"])
            if data["ISOSEQ3_COLLAPSE_PARAMS"]["5p_threshold"]:
                pstr += " --max-5p-diff " + str(data["ISOSEQ3_COLLAPSE_PARAMS"]["5p_threshold"])
            if data["ISOSEQ3_COLLAPSE_PARAMS"]["3p_threshold"]:
                pstr += " --max-3p-diff " + str(data["ISOSEQ3_COLLAPSE_PARAMS"]["3p_threshold"])

            return pstr
        
        except yaml.YAMLError as exc:
            print(exc + "\n SOMETHING IS WRONG WITH THE ISOSEQ3 COLLAPSE CONFIG")


def get_minimap_param_string():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            pstr = " " 
            
            try: 
                pstr = data["MINIMAP2_CUSTOM"]
                return pstr
            except:
                pass 
            
            if data["MINIMAP2_PARAMS"]["preset"]:
                pstr += " -x" + data["MINIMAP2_PARAMS"]["preset"]  
            if data["MINIMAP2_PARAMS"]["secondary_alignments"] == False:
                pstr += " --secondary=no "
            if data["MINIMAP2_PARAMS"]["canon_spl_strand"]=="transcript":
                pstr += " -uf "
            elif data["MINIMAP2_PARAMS"]["canon_spl_strand"]=="both": 
                pstr += " -ub " 
        
            else:
                pstr += " -un " 
            
            return pstr + " -a "
        
        except yaml.YAMLError as exc:
            print(exc + "\n SOMETHING IS WRONG WITH THE MINIMAP2 CONFIG")




def get_pbmm2_param_string():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            pstr = " " 
            
            try: 
                pstr = data["PBMM2_CUSTOM"]
                return pstr
            except:
                pass 

            if data["PBMM2_PARAMS"]["sort_output_bam"] == True:
                pstr += " --sort "
            
            if data["PBMM2_PARAMS"]["preset"]:
                pstr += " --preset " + data["PBMM2_PARAMS"]["preset"]
            
            return pstr 
        
        except yaml.YAMLError as exc:
            print(exc + "\n SOMETHING IS WRONG WITH THE PBMM2 CONFIG")

def get_stringtie_param_string():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            pstr = " " 
            
            try: 
                pstr = data["STRINGTIE_CUSTOM"]
                return pstr
            except:
                pass 
            
            return pstr + " -L "

        
        except yaml.YAMLError as exc:
            print(exc + "\n SOMETHING IS WRONG WITH THE PBMM2 CONFIG")

def get_stringtie_reference():
    with open("config/config.yaml", "r") as stream:
        try:
            data = yaml.safe_load(stream)
            return " -G "+ "resources/" +data["STRINGTIE_REFERENCE"]
        except:
            return " "