sys.path.insert(0, Path(workflow.basedir).as_posix())
from scripts.param_string import get_isoseq_collapse_param_string, get_stringtie_param_string, get_stringtie_reference


rule stringtie:
    input: 
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sort.bam"
    output:
        "results/structural_annotation/{output_name}_stringtie_assembly.gtf"
    log:
        "logs/{output_name}_stringtie.log"
    benchmark:
        "benchmarks/{output_name}_stringtie_pbmm2_no_ref.benchmark.txt" 
    conda:
        "../envs/snakemake.yaml"
    params: 
        param_string = get_stringtie_param_string(),
        ref = get_stringtie_reference()
    shell: 
        "stringtie  {params.param_string} {params.ref}  {input} -o {output} 1>&2>{log}"




rule iso_seq_collapse:
    input:
        "results/mapped_reads/{output_name}_pbmm2_mapped_reads.bam"
    output:
        "results/structural_annotation/{output_name}_iso_seq_output/{output_name}_iso_seq_collapse.gff"
    benchmark:
        "benchmarks/{output_name}_iso_seq_collapse.benchmark.txt" 
    log:
        "logs/{output_name}_iso_seq_collapse.log"
    conda:
        "py27tama"
    params: 
        param_string = get_isoseq_collapse_param_string()
    shell:
        "isoseq3 collapse {params.param_string} {input} {output} 1>&2>{log}"


