sys.path.insert(0, Path(workflow.basedir).as_posix())
from scripts.param_string import get_TAMA_Collapse_param_string


rule tama_collapse_single: 
    input: 
        bam           = "results/mapped_reads/{output_name}_minimap_mapped_reads.sort.bam", 
        reference     = input_reference
    output: 
        output_folder = directory("results/structural_annotation/TAMA_collapse_unmerged_{output_name}/"),
        bed           = "results/structural_annotation/TAMA_collapse_unmerged_{output_name}/{output_name}.bed"
    params:
        prefix =        "results/structural_annotation/TAMA_collapse_unmerged_{output_name}/{output_name}",
        param_string = get_TAMA_Collapse_param_string()
    log:
        "logs/{output_name}_tama_collapse.log"
    benchmark:
        #"benchmarks/{output_name}_tama_single.benchmark.txt"
        "benchmarks/{output_name}_tama_single_pbmm2.benchmark.txt" 
    conda:
        "../envs/py27tama.yaml"
    shell:
        """ 
        python tools/tama/tama_collapse.py \
        {params.param_string} \
        -s {input.bam} -f {input.reference} -p {params.prefix} 
        2>{log}
        """ 


rule samtools_sort_sam_to_bam: 
    input: 
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sam"
    output:  
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sort.bam"
    log:
        "logs/{output_name}_samtools_sorting_bam.log"
    conda:
        "../envs/snakemake.yaml"
    shell:
        "samtools sort -O bam -o {output} {input}"


