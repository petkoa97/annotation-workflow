sys.path.insert(0, Path(workflow.basedir).as_posix())
from scripts.param_string import get_TAMA_Collapse_param_string


rule samtools_sorting_sam:
    input: 
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sam"
    output: 
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sort.sam"
    log:
        "logs/{output_name}_samtools_sorting_bam.log"
    conda:
        "../envs/snakemake.yaml"
    shell:
        "samtools sort -O sam {input} -o {output} 1>&2>{log}"



checkpoint split_sam:
    input:
        "results/mapped_reads/{output_name}_minimap_mapped_reads.sort.sam"
    output:
        dire = temp(directory("results/mapped_reads/{output_name}_split_sams/")),
    log:
        "logs/{output_name}_split_sam.log"
    params: 
        chunk_size= config["TAMA_split_chunk_size_in_thousands"]
    conda:
        "../envs/py27tama.yaml"
    shell:
        '''
        python workflow/scripts/sam_splitter.py {input} {output.dire} {wildcards.output_name} {params.chunk_size} 1>&2>{log}
        '''
   

    
rule sam_to_bam:
    input: 
        "results/mapped_reads/{output_name}_split_sams/{output_name}_{split_index}.sam"
    output:
        bam = temp("results/mapped_reads/{output_name}_split_bams/{output_name}_{split_index}.bam")
    conda:
        "../envs/snakemake.yaml"
    log:
        "logs/{output_name}_{split_index}_sam_to_bam.log"
    shell:
        "samtools view -b {input} -o {output.bam} 1>&2>{log}"
   
    



rule tama_collapse_split: 
    input: 
        bam           = "results/mapped_reads/{output_name}_split_bams/{output_name}_{split_index}.bam", 
        reference     = input_reference,
    output: 
        bed           = "results/structural_annotation/TAMA_collapse_split_{output_name}_{split_index}/{output_name}_{split_index}.bed",
        output_folder = temp(directory("results/structural_annotation/TAMA_collapse_split_{output_name}_{split_index}/")),
    params:
        prefix        = "results/structural_annotation/TAMA_collapse_split_{output_name}_{split_index}/{output_name}_{split_index}",
        param_string  = get_TAMA_Collapse_param_string()
    benchmark:
        "benchmarks/{output_name}_tama_split_{split_index}.benchmark.txt" 
    log:
        "logs/{output_name}_{split_index}_tama_collapse.log"
    conda:
        "../envs/py27tama.yaml"
    shell:
        """
        python tools/tama/tama_collapse.py \
        {params.param_string} \
        -s {input.bam} -f {input.reference} -p {params.prefix} 
        1>&2>{log}
        """ 




def aggregate_input_beds(wildcards):
    checkpoint_output = checkpoints.split_sam.get(**wildcards).output[0]
    return ( expand("results/structural_annotation/TAMA_collapse_split_{output_name}_{split_index}/{output_name}_{split_index}.bed",
        output_name=wildcards.output_name, split_index=glob_wildcards(os.path.join(checkpoint_output, "{output_name}_{split_index}.sam")).split_index))



def aggregate_input_dirs(wildcards):
    checkpoint_output = checkpoints.split_sam.get(**wildcards).output[0]
    return ( expand("results/structural_annotation/TAMA_collapse_split_{output_name}_{split_index}/",
        output_name=wildcards.output_name, split_index=glob_wildcards(os.path.join(checkpoint_output, "{output_name}_{split_index}.sam")).split_index))




rule merge_file_list:
    input: 
        aggregate_input_beds
    output: 
        "results/structural_annotation/TAMA_merge_file_list_{output_name}.txt"
    log:
        "logs/{output_name}_merge_file_list.log"
    conda:
        "../envs/snakemake.yaml"
    shell:
        "python workflow/scripts/merge_indexing.py '{input}' {output} 1>&2>{log}"




rule TAMA_merge:
    input:
        index = "results/structural_annotation/TAMA_merge_file_list_{output_name}.txt",
        dirs  = aggregate_input_dirs
    output:
        bed = "results/structural_annotation/TAMA_collapse_merged_{output_name}/{output_name}.bed"
    log:
        "logs/{output_name}_tama_merge.log"
    conda:
        "../envs/py27tama.yaml"
    params:
        prefix = "results/structural_annotation/TAMA_collapse_merged_{output_name}/{output_name}"
    shell:
        '''
        python tools/tama/tama_merge.py -f {input.index} -p {params.prefix} 1>&2>{log}\

        '''

