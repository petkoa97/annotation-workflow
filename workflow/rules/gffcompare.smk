rule convert_bed_gtf:
    input:
        "results/structural_annotation/{tama}_{output_name}/{output_name}.bed"
    output:
        "results/structural_annotation/{tama}_{output_name}/{output_name}.gtf"
    log:
        "logs/{output_name}_{tama}_convert_bed_gtf.log"
    conda:
        "../envs/py27tama.yaml"
    shell:
        "python tools/tama/tama_go/format_converter/tama_convert_bed_gtf_ensembl_no_cds.py {input} {output} 1>&2>{log}"


def get_output_files(wildcards):
    input_list=[]
    
    if config["RUN_TAMA_COLLAPSE"] and config["TAMA_split_input"]:
        input_list.append("results/structural_annotation/TAMA_collapse_merged_{output_name}/{output_name}.gtf") 
    # if config["RUN_TAMA_COLLAPSE"] and not config["TAMA_split_input"]:    # bugs with wildcards
    #     input_list.append("results/structural_annotation/TAMA_collapse_unmerged_{output_name}/{output_name}.gtf")
    if config["RUN_ISO_SEQ_COLLAPSE"]:
        input_list.extend(rules.iso_seq_collapse.output)
    if config["RUN_STRINGTIE"]:
        input_list.extend(rules.stringtie.output) 

    return input_list

rule compare_gtf:
    input:  
        get_output_files
    output:
        "results/compare/{output_name}/gffcmp.stats"
    log:
        "logs/{output_name}_compare_gtf.log"
    params:
        output_name = config["output_name_prefix"],
    conda:
        "../envs/gffcompare.yaml"
    shell: 
        "gffcompare {input} -o {output} 1>&2>{log}"