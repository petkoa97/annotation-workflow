sys.path.insert(0, Path(workflow.basedir).as_posix())
from scripts.param_string import get_minimap_param_string, get_pbmm2_param_string 

rule minimap2:
    input: 
        reads     = input_HiFi_reads,
        reference = input_reference
    output:
        temp("results/mapped_reads/{output_name}_minimap_mapped_reads.sam")
    log:
        "logs/{output_name}_minimap.log"
    benchmark:
        "benchmarks/{output_name}_minimap.benchmark.txt" 
    conda:
        "../envs/snakemake.yaml"
    params:
        param_string = get_minimap_param_string()
    threads:
        workflow.cores
    shell:
        "minimap2 {params.param_string} -t {threads} {input.reference} {input.reads} > {output} 2>{log}"



rule pbmm2_align:
    input: 
        reads     = input_HiFi_reads,
        reference = input_reference
    output:
        "results/mapped_reads/{some_name}_pbmm2_mapped_reads.bam"
    log:
        "logs/{some_name}_pbmm2_align.log"
    benchmark:
        "benchmarks/{some_name}_pbmm2.benchmark.txt" 
    conda:
        "../envs/py27tama.yaml"
    params:
        param_string = get_pbmm2_param_string()
    threads:
        workflow.cores
    shell:
        "pbmm2 align {params.param_string} -j {threads} {input.reads} {input.reference} {output} 1>&2>{log}"


