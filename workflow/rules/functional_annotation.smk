

rule bedtools_bed_to_fasta:
    input:
        bed = "results/structural_annotation/TAMA_collapse_merged_{output_name}/{output_name}.bed",
        ref = input_reference
    output:
        fasta =  "results/functional_annotation/{output_name}/{output_name}.fasta"
    conda: 
        "../envs/py27tama.yaml"
    log:
        "logs/bedtools_bed_to_fasta_{output_name}.log"
    shell:
        "bedtools getfasta -name -split -s -fi {input.ref} -bed {input.bed} -fo {output.fasta} 1>&2>{log}"

rule fasta_orf_seeker:
    input:
        fasta =  "results/functional_annotation/{output_name}/{output_name}.fasta"
    output:
        orf = "results/functional_annotation/{output_name}/{output_name}_orf.fasta"
    log:
        "logs/{output_name}_fasta_orf_seeker.log"
    conda:
        "../envs/py27tama.yaml"
    shell:
        "python tools/tama/tama_go/orf_nmd_predictions/tama_orf_seeker.py -f {input.fasta} -o {output.orf} 1>&2>{log}"



rule blastp:
    input:
        orf = "results/functional_annotation/{output_name}/{output_name}_orf.fasta",
        db = "tools/" + config["BLAST_DATABASE"]
    output:
        "results/functional_annotation/{output_name}/{output_name}_blastp.txt"
    benchmark:
        "benchmarks/blastp_{output_name}.benchmark.txt"
    threads: 
        workflow.cores
    conda:
        "../envs/blast.yaml"
    log:
        "logs/blastp_{output_name}.log"
    shell:
        "blastp -evalue 1e-10 -num_threads {threads} -db {input.db} -query {input.orf} -out {output} 1>&2>{log}"
        

rule parse_top_hits:
    input: 
        "results/functional_annotation/{output_name}/{output_name}_blastp.txt"
    output:
        "results/functional_annotation/{output_name}/{output_name}_blastp_parsed.txt"
    benchmark:
        "benchmarks/parse_top_hits_{output_name}.benchmark.txt"
    log:
        "logs/parse_top_hits_{output_name}.log"
    conda:
        "../envs/py27tama.yaml"
    shell:
        "python tools/tama/tama_go/orf_nmd_predictions/tama_orf_blastp_parser.py -b {blastp} -o {outfile} 1>&2>{log}"

rule create_cds_bed:
    input: 
        parsed = "results/functional_annotation/{output_name}/{output_name}_blastp_parsed.txt",
        bed    = "results/structural_annotation/TAMA_collapse_merged_{output_name}/{output_name}.bed",
        ref    = input_reference,
    output:
        outfile = "results/functional_annotation/{output_name}/{output_name}_final.bed"
    benchmark:
        "benchmarks/create_cds_bed_{output_name}.benchmark.txt"
    log:
        "logs/create_cds_bed_{output_name}.log"
    conda:
        "../envs/py27tama.yaml"
    shell:
        "python tools/tama/tama_go/orf_nmd_predictions/tama_cds_regions_bed_add.py -p {input.parsed} -a {input.bed} -f {input.ref} -o {outfile} 1>&2>{log}"