# Iso-seq annotation workflow

This repository houses the code of my bachelor thesis.

  

# Abstract

  

Integrating the most recent technologies in a compact and efficient way is essential to improving productivity in the lab. The following work seeks to chain several of the most popular tools for genome annotation using long read Iso-Seq RNA data and automate the computation of structural and functional annotation in a single configurable workflow. This is accomplished using Snakemake - a powerful python based workflow management system.


# Installation
To successfully execute this workflow you need a current version of Snakemake and Anaconda Python. Afther that, clone this repo and run Snakemake from the top directory or run the "run_snakemake.sh" bash script. Most necessary files will be installed automatically by conda with exception of a Blast database which needs to be manually configured and passed in the config file. 

# Manual

 ### 1.  Configuration 
 All tools and packages are meant to be configured in the `config/config.yaml` file. Selecting the tools to be run is done by setting the boolean values adjacent to them to `True`. Selecting the desired structural analysis tool will run the necessary Mapping step automatically.
Further configuration of the selected tools is done further down in the configuration file with each tool having a dictionary based interface to enter the parameters to be used. Additionally a custom string can be specified for each tool which, if detected, will be prioritized over the dictionary. 

  ### 2.  Input/Output files
 All Input and Output files are also supplied in the configuration file with their relative paths starting from the `resources/` directory and in the case of tools (such as blast) in the `tools` directory.

  ### 3.  Executing 
  Executing Snakemake can be done via calling the `run_snakemake.sh` script or manually by running Snakemake in the main  directory of this project. Using conda is essential for this!  (for example `snakemake --cores 10 --use-conda`)

  ### 4.  Optimization 
  Mappers and Blast will take all supplied cores by default. Most other tools can only use one thread at a time. Memory usage and other resources are not restricted by default. 

 